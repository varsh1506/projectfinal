document.addEventListener("DOMContentLoaded", function () {
    const sections = document.querySelectorAll("section");
    sections.forEach((section) => {
      if (section.id !== "home") {
        section.style.display = "none";
      }
    });
  
    function showSection(sectionId) {
      sections.forEach((section) => {
        if (section.id === sectionId) {
          section.style.display = "block";
        } else {
          section.style.display = "none";
        }
      });
    }
  
    const navLinks = document.querySelectorAll("nav a");
    navLinks.forEach((link) => {
      link.addEventListener("click", function (event) {
        event.preventDefault();
        const targetSectionId = link.getAttribute("href").substring(1);
        showSection(targetSectionId);
      });
    });
  });
  